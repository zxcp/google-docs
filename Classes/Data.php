<?php /** Created by Anton on 23.04.2019. */

namespace Classes;

class Data {
    public static function getFromPOST($post) {
        $data = [];
        foreach (DB::FIELDS as $field) {
            $data[] = isset($post[$field]) ? $post[$field] : 0;
        }
        return $data;
    }
}