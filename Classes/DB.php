<?php /** Created by Anton on 23.04.2019. */

namespace Classes;

use PDO;

class DB
{
    const HOST = '127.0.0.1';
    const DB = 'nutnet';
    const USER = 'root';
    const PASS = 'root';
    const TABLE = 'humans';
    const FIELDS = [
        'name',
        'surname',
        'age'
    ];

    protected static $_pdo = null;

    /** @return null|PDO */
    public static function get()
    {
        if (self::$_pdo === null) {
            $dsn = 'mysql:host=' . self::HOST . ';dbname=' . self::DB . ';charset=utf8';
            $opt = [
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES   => false,
            ];
            self::$_pdo = new PDO($dsn, self::USER, self::PASS, $opt);
        }
        return self::$_pdo;
    }

    public static function getData()
    {
        $sql = 'SELECT `' . implode('`, `', self::FIELDS) . '` FROM ' . DB::TABLE . ' WHERE age > 18';
        $values = [];
        foreach (DB::get()->query($sql) as $row) {
            $rowValues = [];
            foreach (self::FIELDS as $field) {
                $rowValues[] = $row[$field];
            }
            $values[] = $rowValues;
        };

        return $values;
    }

    public static function insert()
    {
        $smtp = self::get()->prepare('INSERT INTO ' . self::TABLE .
            ' (`' . implode('`, `', self::FIELDS) . '`)' .
            ' VALUE (?, ?, ?)');

        echo $smtp->execute(Data::getFromPOST($_POST));
    }
}
