<?php /** Created by Anton on 24.04.2019. */

namespace Classes;

require '../vendor/autoload.php';

class GoogleSheets {
    const SPREADSHEET_ID = '1B6EidcKHxirlIQ9PCxnx-r4OhIBbs_hQsb674JXgljo';
    const RANGE = 'page1!A2';
    const SECRET_FILE = '../nutnet.json';
    const TOKEN_FILE = '../token.json';
    private $service = null;

    public function __construct()
    {
        $this->service = new \Google_Service_Sheets($this->getClient());
    }

    /**
     * Обновляет данные таблицы
     * @param array $values
     */
    public function update($values)
    {
        $this->clear();

        $body = new \Google_Service_Sheets_ValueRange( [ 'values' => $values ] );
        $options = ['valueInputOption' => 'RAW'];
        $this->service->spreadsheets_values->update( self::SPREADSHEET_ID, self::RANGE, $body, $options );
    }

    /**
     * Возвращает данные из таблицы
     * @return mixed
     */
    public function getData()
    {
        $response = $this->service->spreadsheets_values->get(
            self::SPREADSHEET_ID,
                self::RANGE
        );
        return $response->getValues();
    }

    /** Очищает таблицу */
    private function clear()
    {
        $this->service->spreadsheets_values->clear(
            self::SPREADSHEET_ID,
                self::RANGE . ':C',
                new \Google_Service_Sheets_ClearValuesRequest([]));
    }

    /**
     * Returns an authorized API client.
     * @return /Google_Client the authorized client object
     */
    private function getClient()
    {
        $client = new \Google_Client();
        $client->setApplicationName('Google Sheets API PHP humans');
        $client->setScopes(\Google_Service_Sheets::SPREADSHEETS);
        $client->setAuthConfig(self::SECRET_FILE);
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');

        if (file_exists(self::TOKEN_FILE)) {
            $accessToken = json_decode(file_get_contents(self::TOKEN_FILE), true);
            $client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(STDIN));

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new \Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname(self::TOKEN_FILE))) {
                mkdir(dirname(self::TOKEN_FILE), 0700, true);
            }
            file_put_contents(self::TOKEN_FILE, json_encode($client->getAccessToken()));
        }
        return $client;
    }
}
