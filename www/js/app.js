/** Created by Anton on 23.04.2019. */

$(document).ready( function () {
    //Кнопка сохранить
    $('.js-save').on('click', function () {
        $.ajax({
            url: 'save.php',
            type: 'post',
            data: $('form').serializeArray(),
            success: showMgs('Запись добавлена', false),
            error: function(e) {
                showMgs('Ошибка: ' + e.status + ' ' + e.statusText, true)
            }
        });
    });
    //Кнопка выгрузить
    $('.js-upload').on('click', function () {
        $.ajax({
            url: 'upload.php',
            type: 'get',
            success: showMgs('Записи выгружены', false),
            error: function(e) {
                showMgs('Ошибка: ' + e.status + ' ' + e.statusText, true)
            }
        });
    });
    /**
     * Выводит сообщение
     * @var string msg - выводимое сообщение
     * @var bool error - true - выведет сообщение красным цветом | false - зелёным
     */
    function showMgs(msg, error) {
        $('form').before(
            '<div class="' + (error ? 'error' : 'success') + ' text-center">' +
            msg + '</div>'
        )
    }
});
