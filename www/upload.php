<?php /** Created by Anton on 23.04.2019. */

use Classes\DB;
use Classes\GoogleSheets;

require '../vendor/autoload.php';

spl_autoload_register(function ($class_name) {
    include '../' . $class_name . '.php';
});

$values = DB::getData();

(new GoogleSheets())->update($values);
